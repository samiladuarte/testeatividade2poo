package br.ucsal.bes20211.poo.atividadepontuada2.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

//nome, telefone, endere�o e data de anivers�rio.

public class Contato {

	private String nome;

	private String telefone;

	private String endereco;

	private String aniversario;
	
	
	

	public Contato() {
	}

	public Contato(String nome, String telefone, String endereco, String aniversario) {
		this.nome = nome;
		this.telefone = telefone;
		this.endereco = endereco;
		this.aniversario = aniversario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getAniversario() {
		return aniversario;
	}

	public void setAniversario(String aniversario) {
		this.aniversario = aniversario;
	}

	public String toString() {
		return ">Contato: " + nome + "\n>Telefone: " + telefone + "\n>Endere�o: " + endereco + "\n>Anivers�rio: "
				+ aniversario;
	}

}
