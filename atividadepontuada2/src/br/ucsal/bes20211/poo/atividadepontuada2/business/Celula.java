package br.ucsal.bes20211.poo.atividadepontuada2.business;

import br.ucsal.bes20211.poo.atividadepontuada2.domain.Contato;

public class Celula {
	private Contato valor;
	private Celula proximo;

	public Contato getValor() {
		return valor;
	}

	public void setValor(Contato valor) {
		this.valor = valor;
	}

	public Celula getProximo() {
		return proximo;
	}

	public void setProximo(Celula proximo) {
		this.proximo = proximo;
	}

}
