package br.ucsal.bes20211.poo.atividadepontuada2.tui;

import java.util.Scanner;

import br.ucsal.bes20211.poo.atividadepontuada2.domain.Contato;
import br.ucsal.bes20211.poo.atividadepontuada2.persistence.ListaEncadeada;

public class Agenda {

	static void menuInicial() {
		Scanner sc = new Scanner(System.in);
		System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
		System.out.println("                   AGENDA                      ");
		System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
		System.out.println("Selecione a op��o:\n" + "\n" + "1. Cadastro\n" + "2. Listagem\n" + "3. Pesquisa por nome");

		int op = sc.nextInt();
		switch (op) {
		case 1:
			cadastro();
			break;
		case 2:
			lista();
			break;
		case 3:
			// pesquisa();
			break;
		default:
			System.out.println("Op��o Invalida!");
			menuInicial();
			break;
		}
	}

	public static void cadastro() {

		ListaEncadeada listaEncadeada = new ListaEncadeada();
		adicionarContato(listaEncadeada);
		while (listaEncadeada.temProximo()) {
			menuInicial();
		}

	}

	public static void lista() {

		ListaEncadeada listaEncadeada = new ListaEncadeada();
		System.out.println(" ");
		System.out.println("> A lista tem os seguintes contatos <");
		System.out.println(" ");

		while (listaEncadeada.temProximo()) {
			System.out.println(listaEncadeada.getPosicaoAtual().getValor().toString());
		}

	}

	public static void adicionarContato(ListaEncadeada listaEncadeada) {
		Scanner sc = new Scanner(System.in);

		boolean op;

		do {
			System.out.println("> Cadastro <");
			System.out.println("Informe o nome:");
			String nomeContato = sc.nextLine();

			System.out.println("Informe o telefone:");
			String telefoneContato = sc.nextLine();

			System.out.println("Informe o endereco:");
			String endereco = sc.nextLine();

			System.out.println("Informe o anivers�rio (dd/mm/aaaa)");
			String aniversario = sc.nextLine();

			listaEncadeada.adicionar(new Contato(nomeContato, telefoneContato, endereco, aniversario));

			System.out.print("Deseja cadastrar um outro contato? s/n: ");

			char resp = sc.nextLine().charAt(0);

			op = (resp == 's') ? true : false;
		} while (op);

		sc.close();
	}

}
